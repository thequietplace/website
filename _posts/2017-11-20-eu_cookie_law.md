---
title: EU Cookie Law
tags: [misc]
---

# EU Cookie Law

Today we added support for cookie notifications to help with EU Cookie Law compliance. I used [this (link)](https://cookieconsent.insites.com/download/) for the compliance. I'd like to be rid of the pop-up but... I'd rather be in compliance than not.
