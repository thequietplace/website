---
title: Updated Block List
tags: [block,suspend,admin]
---

Today we updated the instance block list and reject media list for *npf.mlpol.net*. It's a GNU Social instance with the fillowing title/tagline. I think we'll survive without them in our timelines.

compassion.online recently added them to their main list of toxic instances and we followed their lead.

Title
: Nazi Pony Fucker Social

Tagline
: Welcome to the cross roads where political incorrectness and cartoon horse p****y meet!
