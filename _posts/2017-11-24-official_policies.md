---
title: Official Policies Posted
tags: [misc]
---

# Official Policies

The official instance policies are now live on the website *and* the instance about/more page. Check them out [here (link)](https://thequietplace.social/misc/policies) or [here (link)](https://mastodon.thequietplace.social/about/more).

If you'd like to see them expanded or have suggestions, please let [@Ambassador](https://mastodon.thequietplace.social/@Ambassador) know.
