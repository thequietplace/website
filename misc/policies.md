---
layout: page
title: Policies
subtitle: Official Instance Policies
---

{:.no_toc}
* TOC
{:toc}

# Federation/Follow Bot Policy
 
If you run a federation bot or a follow bot. Please prevent it from indexing thequietplace.social users. 
 
We are here because it is an escape from the normal bustle of larger instances. We'd prefer to stay out of the limelight and out of timelines unless explicitly followed by individuals.
 
# Follow Friday Policy
 
Given this instance is something of a 'Quiet place in the woods' for users. We politely ask that you do not tag our users in #FollowFriday posts. If you're not sure, don't tag our users is a good rule of thumb. Some of our users have even gone so far as to add #NoFF to their profiles to be very clear they don't want to be tagged.
 
We respectfully ask that you honor this policy when it comes to #FollowFriday.
 
# Blocked/Silenced/No Remote Media Policy 
 
As a general rule this instance follows the instance list published by compassion.online at https://compassion.online/the_list 
 
The list is comprehensive and we use it as a baseline for blocks, silences and media rejections. We also encourage our users to report instances via the form on the compassion list page. 
 
Please reach out to [@Ambassador](https://mastodon.thequietplace.social/@Ambassador) if you would like to discuss any blocks/silences/media rejections further. The door is always open and discourse welcome. 
 
# New User Policy
 
If you know an individual who'd find this instance a good home, please let [@Ambassador](https://mastodon.thequietplace.social/@Ambassador) know.
 
@Ambassador will post the invite to the instance public timeline and users of the instance will have 5 calendar days to vote yay or nay on the invitation. If a majority of users vote 'yay' an invite will be extended to the nomiated user. @Ambassador will work with the user to get registered and setup.
